package com.tsystems.controllers;

import com.tsystems.dto.Address;
import com.tsystems.service.AddressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/address")
public class AddressController {

    private final Logger log = LoggerFactory.getLogger(AddressController.class);

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    public String displayPage(Model model) {
        model.addAttribute("address", new Address());
        return "addresses";
    }

    @PostMapping
    public String addAddress(@Valid @ModelAttribute("address") Address address,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addresses";
        }

        addressService.createAddress(address);
        return "redirect:/profile";
    }

    @GetMapping("/edit/{id}")
    public String displayEditPage(@PathVariable("id") final Integer addressId,
                                  Model model) {
        Address address = addressService.findById(addressId);
        model.addAttribute("address", address);
        return "edit-address";
    }


    @PostMapping("/edit")
    public String editAddress(@Valid @ModelAttribute("address") Address address,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addresses";
        }

        addressService.updateAddress(address);
        return "redirect:/profile";
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAddress(@PathVariable("id") final Integer id) {
        addressService.delete(id);
        return ResponseEntity.ok().build();
    }

}
