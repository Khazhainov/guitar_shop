package com.tsystems.controllers;

import com.tsystems.dto.Product;
import com.tsystems.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminProfileController {

    private final ProductService productService;

    @Autowired
    public AdminProfileController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public String showProductForm(Model model){
        model.addAttribute("product", new Product());
        return "admin-profile";
    }

    @PostMapping
    public String registerNewProduct(@Valid @ModelAttribute("product") Product product, BindingResult result){
        if(result.hasErrors()){
            return "admin-profile";
        }
        productService.createProduct(product);
        return "admin-profile";
    }
}
