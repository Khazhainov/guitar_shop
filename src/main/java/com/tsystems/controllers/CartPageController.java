package com.tsystems.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsystems.dto.Cart;
import com.tsystems.dto.CartProduct;
import com.tsystems.dto.CartProducts;
import com.tsystems.service.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;

@Controller
@RequestMapping("/cart")
public class CartPageController {

    private final Logger log = LoggerFactory.getLogger(CartPageController.class);

    private final CartService cartService;

    @Autowired
    public CartPageController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping
    public String display(@CookieValue(name = "cart", required = false) final String json,
                          Model model) throws IOException {
        Cart cart = new Cart(Collections.emptyList(), 0.0d);

        if (json != null && !json.trim().isEmpty()) {
            ObjectMapper mapper = new ObjectMapper();
            CartProducts cartProducts = mapper.readValue(json, CartProducts.class);
            cart = cartService.getProductsFromCart(cartProducts);
        }

        model.addAttribute("cart", cart);
        return "cart";
    }

    @PostMapping("/recalculate")
    public @ResponseBody Cart recalculateCart(@RequestBody CartProducts cartProducts) {
        return cartService.getProductsFromCart(cartProducts);
    }

}
