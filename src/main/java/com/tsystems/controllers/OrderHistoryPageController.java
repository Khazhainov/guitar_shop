package com.tsystems.controllers;

import com.tsystems.service.OrderService;
import com.tsystems.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/history")
public class OrderHistoryPageController {

    private final Logger log = LoggerFactory.getLogger(OrderHistoryPageController.class);

    private static final String ANONYMOUS = "anonymousUser";

    private final OrderService orderService;
    private final UserService userService;

    @Autowired
    public OrderHistoryPageController(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    @GetMapping
    public String displayPage(Model model){
        model.addAttribute("orders",orderService.findAll());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!ANONYMOUS.equals(auth.getName())) {
            log.error(auth.getPrincipal() + "");
            User principal = (User) auth.getPrincipal();
            model.addAttribute("user", userService.findByEmail(principal.getUsername()));
        }

        return "order-history";
    }
}
