package com.tsystems.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsystems.dto.Cart;
import com.tsystems.dto.CartProducts;
import com.tsystems.dto.Order;
import com.tsystems.dto.User;
import com.tsystems.service.AddressService;
import com.tsystems.service.AuthenticationService;
import com.tsystems.service.CartService;
import com.tsystems.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;


@Controller
@RequestMapping("/order")
public class OrderPageController {

    private final CartService cartService;
    private final AuthenticationService authService;
    private final AddressService addressService;
    private final OrderService orderService;

    @Autowired
    public OrderPageController(CartService cartService, AuthenticationService authService, AddressService addressService, OrderService orderService) {
        this.cartService = cartService;
        this.authService = authService;
        this.addressService = addressService;
        this.orderService = orderService;
    }

    @GetMapping
    public String display(@CookieValue(name = "cart", required = false) final String json,
                          Model model) throws IOException {
        if (json == null || json.trim().isEmpty()) {
            model.addAttribute("error", "Could not create order, when cart is empty. Please, fill the cart first");
            return "order";
        }

        ObjectMapper mapper = new ObjectMapper();
        CartProducts cartProducts = mapper.readValue(json, CartProducts.class);
        Cart cart = cartService.getProductsFromCart(cartProducts);

        if (cart == null || cart.getProducts() == null || cart.getProducts().isEmpty()) {
            model.addAttribute("error", "Could not create order, when cart is empty. Please, fill the cart first");
            return "order";
        }

        User user = authService.getAuthenticatedUser();
        model.addAttribute("user", user);
        model.addAttribute("addresses", addressService.findUserAddresses(user));
        model.addAttribute("cart", cart);

        return "order";
    }

    @PostMapping("/create")
    private String createOrder(@Valid @ModelAttribute("newOrder")Order order, BindingResult result){
        if(result.hasErrors()){
            return "order";
        }
        orderService.createNewOrder(order);
        return "redirect:/products";
    }



}
