package com.tsystems.controllers;

import com.tsystems.exception.EmptyStringException;
import com.tsystems.service.AuthenticationService;
import com.tsystems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/change/password")
public class PasswordController {

    private final UserService userService;
    private final AuthenticationService authService;

    @Autowired
    public PasswordController(UserService userService, AuthenticationService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @GetMapping
    public String displayPage() {
        return "change-password";
    }

    @PostMapping
    public String changePassword(@RequestParam("password") final String password, Model model) {
        try {
            userService.changePassword(authService.getAuthenticatedUser(), password);
            return "redirect:/profile";
        } catch (EmptyStringException e) {
            model.addAttribute("error", e.getMessage());
            return "change-password";
        }
    }

}
