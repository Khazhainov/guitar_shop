package com.tsystems.controllers;

import com.tsystems.enums.Brand;
import com.tsystems.enums.Color;
import com.tsystems.service.CategoryService;
import com.tsystems.service.ProductService;
import com.tsystems.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/products")
public class ProductPageController {

    private final Logger log = LoggerFactory.getLogger(ProductPageController.class);

    private static final String ANONYMOUS = "anonymousUser";

    private final ProductService productService;
    private final CategoryService categoryService;
    private final UserService userService;

    @Autowired
    public ProductPageController(ProductService productService, CategoryService categoryService, UserService userService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.userService = userService;
    }

    @GetMapping
    public String displayPage(Model model){
        model.addAttribute("brands", Brand.values());
        model.addAttribute("models", productService.findAll());
        model.addAttribute("colors", Color.values());
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("products", productService.findAll());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!ANONYMOUS.equals(auth.getName())) {
            log.error(auth.getPrincipal() + "");
            User principal = (User) auth.getPrincipal();
            model.addAttribute("user", userService.findByEmail(principal.getUsername()));
        }
        return "products";
    }


}
