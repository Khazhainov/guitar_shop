package com.tsystems.controllers;

import com.tsystems.service.OrderService;
import com.tsystems.service.ProductService;
import com.tsystems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/statistics")
public class SalesStatisticsPageController {

    private final UserService userService;
    private final ProductService productService;
    private final OrderService orderService;

    @Autowired
    public SalesStatisticsPageController(UserService userService, ProductService productService, OrderService orderService) {
        this.userService = userService;
        this.productService = productService;
        this.orderService = orderService;
    }

    @GetMapping
    public String displayPage(Model model){
        model.addAttribute("topClients",userService.getTopTenClients());
        model.addAttribute("topProducts",productService.getTopTenProducts());
        return "statistics";
    }



}
