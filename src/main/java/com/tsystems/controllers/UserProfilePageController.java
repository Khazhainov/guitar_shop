package com.tsystems.controllers;

import com.tsystems.dto.User;
import com.tsystems.service.AddressService;
import com.tsystems.service.AuthenticationService;
import com.tsystems.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/profile")
public class UserProfilePageController {

    private final Logger log = LoggerFactory.getLogger(UserProfilePageController.class);

    private static final int USER_ROLE_ID = 2;

    private final AuthenticationService authService;
    private final UserService userService;
    private final AddressService addressService;


    @Autowired
    public UserProfilePageController(AuthenticationService authService, UserService userService, AddressService addressService) {
        this.authService = authService;
        this.userService = userService;
        this.addressService = addressService;
    }

    @GetMapping
    public String displayUserProfile(Model model) {
        User authenticatedUser = authService.getAuthenticatedUser();
        model.addAttribute("user", authenticatedUser);
        model.addAttribute("addresses", addressService.findUserAddresses(authenticatedUser));
        if(authenticatedUser.getRole().getId() == USER_ROLE_ID) return "userProfile";
        else return "admin-profile";
    }

    @GetMapping("/edit")
    public String displayEditForm(Model model) {
        model.addAttribute("user", authService.getAuthenticatedUser());
        return "edit-profile-information";
    }

    @PostMapping("/edit")
    public String editInformation(@Valid @ModelAttribute("user") User user, BindingResult result){
        log.error(user.toString());
        if (result.hasErrors()) {
            return "edit-profile-information";
        }

        userService.updateUser(user);
        return "redirect:/profile";
    }

}
