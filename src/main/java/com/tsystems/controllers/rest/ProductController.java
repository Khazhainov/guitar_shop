package com.tsystems.controllers.rest;

import com.tsystems.dto.Product;
import com.tsystems.dto.SoldProduct;
import com.tsystems.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> list() {
        return productService.findAll();
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable("id") final Integer id) {
        return productService.findById(id);
    }

    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }

//    @PutMapping("/{id}")
//    public Product updateProduct(@PathVariable("id") final Integer id, @RequestBody Product product) {
//        return productService.up
//    }

    @DeleteMapping("/{id}")
    public void removeProduct(@PathVariable("id") final Integer id) {
        productService.deleteById(id);
    }

    @GetMapping("/top")
    public Collection<SoldProduct> getTopTenProducts() {
        return productService.getTopTenProducts();
    }

}
