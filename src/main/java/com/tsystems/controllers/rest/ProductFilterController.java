package com.tsystems.controllers.rest;

import com.tsystems.dto.FilterData;
import com.tsystems.dto.Product;
import com.tsystems.service.ProductFilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/filter")
public class ProductFilterController {

    private final Logger log = LoggerFactory.getLogger(ProductFilterController.class);

    private final ProductFilterService filterService;

    @Autowired
    public ProductFilterController(ProductFilterService filterService) {
        this.filterService = filterService;
    }

    @PostMapping
    public Collection<Product> filter(@RequestBody FilterData filterData) {
        return filterService.filter(filterData);
    }

}
