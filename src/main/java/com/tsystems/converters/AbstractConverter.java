package com.tsystems.converters;

import java.io.Serializable;

public abstract class AbstractConverter<E extends Serializable, D> implements Converter<E, D> {

    @Override
    public D fromEntity(E entity) {
        return entity == null ? null : convertEntity(entity);
    }

    @Override
    public E fromDto(D dto) {
        return dto == null ? null : convertDto(dto);
    }

    protected abstract D convertEntity(E entity);

    protected abstract E convertDto(D dto);

}
