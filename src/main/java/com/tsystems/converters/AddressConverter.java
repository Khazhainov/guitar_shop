package com.tsystems.converters;

import com.tsystems.dto.Address;
import com.tsystems.entity.AddressEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressConverter extends AbstractConverter<AddressEntity, Address> {

    @Override
    public Address convertEntity(AddressEntity entity) {
        Address address = new Address();

        address.setId(entity.getId());
        address.setCountry(entity.getCountry());
        address.setCity(entity.getCity());
        address.setPostIndex(entity.getPostIndex());
        address.setStreet(entity.getStreet());
        address.setHouse(entity.getHouse());
        address.setApartment(entity.getApartment());

        return address;
    }

    @Override
    public AddressEntity convertDto(Address dto) {
        AddressEntity entity = new AddressEntity();

        entity.setId(dto.getId());
        entity.setCountry(dto.getCountry());
        entity.setCity(dto.getCity());
        entity.setPostIndex(dto.getPostIndex());
        entity.setStreet(dto.getStreet());
        entity.setHouse(dto.getHouse());
        entity.setApartment(dto.getApartment());

        return entity;
    }

}