package com.tsystems.converters;

import com.tsystems.dto.Category;
import com.tsystems.entity.CategoryEntity;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter extends AbstractConverter<CategoryEntity, Category> {

    @Override
    public Category convertEntity(CategoryEntity entity) {
        Category category = new Category();

        category.setId(entity.getId());
        category.setName(entity.getName());

        return category;
    }

    @Override
    public CategoryEntity convertDto(Category dto) {
        CategoryEntity entity = new CategoryEntity();

        entity.setId(dto.getId());
        entity.setName(dto.getName());

        return entity;
    }

}
