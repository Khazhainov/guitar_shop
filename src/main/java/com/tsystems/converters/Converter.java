package com.tsystems.converters;

import org.springframework.stereotype.Component;

import java.io.Serializable;

// E - entity
// D - dto
@Component
public interface Converter<E extends Serializable, D> {

    D fromEntity(E entity);

    E fromDto(D dto);

}
