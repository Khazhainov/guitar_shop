package com.tsystems.converters;

import com.tsystems.entity.OrderEntity;
import com.tsystems.dto.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderConverter extends AbstractConverter<OrderEntity, Order> {

    private final UserConverter userConverter;
    private final AddressConverter addressConverter;

    @Autowired
    public OrderConverter(UserConverter userConverter, AddressConverter addressConverter) {
        this.userConverter = userConverter;
        this.addressConverter = addressConverter;
    }

    @Override
    public Order convertEntity(OrderEntity entity) {
        Order order = new Order();

        order.setId(entity.getId());
        order.setUser(userConverter.fromEntity(entity.getUser()));
        order.setAddress(addressConverter.fromEntity(entity.getAddress()));
        order.setPaymentMethod(entity.getPaymentMethod());
        order.setDeliveryMethod(entity.getDeliveryMethod());
        order.setPaymentStatus(entity.getPaymentStatus());
        order.setOrderStatus(entity.getOrderStatus());
        order.setOrderDate(entity.getOrderDate());
        order.setTotalCost(entity.getTotalCost());

        return order;
    }

    @Override
    public OrderEntity convertDto(Order dto) {
        OrderEntity entity = new OrderEntity();

        entity.setId(dto.getId());
        entity.setUser(userConverter.fromDto(dto.getUser()));
        entity.setAddress(addressConverter.fromDto(dto.getAddress()));
        entity.setPaymentMethod(dto.getPaymentMethod());
        entity.setDeliveryMethod(dto.getDeliveryMethod());
        entity.setPaymentStatus(dto.getPaymentStatus());
        entity.setOrderStatus(dto.getOrderStatus());
        entity.setOrderDate(dto.getOrderDate());
        entity.setTotalCost(dto.getTotalCost());

        return entity;
    }
}
