package com.tsystems.converters;

import com.tsystems.dto.Product;
import com.tsystems.entity.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductConverter extends AbstractConverter<ProductEntity, Product> {

    private final CategoryConverter categoryConverter;
    private final ProductDetailsConverter detailsConverter;

    @Autowired
    public ProductConverter(CategoryConverter categoryConverter, ProductDetailsConverter detailsConverter) {
        this.categoryConverter = categoryConverter;
        this.detailsConverter = detailsConverter;
    }

    @Override
    public Product convertEntity(ProductEntity entity) {
        Product product = new Product();

        product.setId(entity.getId());
        product.setName(entity.getName());
        product.setQuantity(entity.getQuantityInStock());
        product.setPrice(entity.getPrice());
        product.setCapacity(entity.getCapacity());
        product.setCategory(categoryConverter.fromEntity(entity.getCategory()));
        product.setDetails(detailsConverter.fromEntity(entity.getDetails()));

        return product;
    }

    @Override
    public ProductEntity convertDto(Product dto) {
        ProductEntity entity = new ProductEntity();

        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setPrice(dto.getPrice());
        entity.setQuantityInStock(dto.getQuantity());
        entity.setCapacity(dto.getCapacity());
        entity.setCategory(categoryConverter.fromDto(dto.getCategory()));
        entity.setDetails(detailsConverter.fromDto(dto.getDetails()));

        return entity;
    }

}
