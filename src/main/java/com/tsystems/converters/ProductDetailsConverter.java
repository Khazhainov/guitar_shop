package com.tsystems.converters;

import com.tsystems.dto.ProductDetails;
import com.tsystems.entity.ProductDetailEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductDetailsConverter extends AbstractConverter<ProductDetailEntity, ProductDetails> {


    @Override
    public ProductDetails convertEntity(ProductDetailEntity entity) {
        ProductDetails details = new ProductDetails();

        details.setBrand(entity.getBrand());
        details.setColor(entity.getColor());
        details.setWeight(entity.getWeight());
        details.setId(entity.getId());

        return details;
    }

    @Override
    public ProductDetailEntity convertDto(ProductDetails dto) {
        ProductDetailEntity entity = new ProductDetailEntity();

        entity.setId(dto.getId());
        entity.setBrand(dto.getBrand());
        entity.setColor(dto.getColor());
        entity.setWeight(dto.getWeight());

        return entity;
    }

}
