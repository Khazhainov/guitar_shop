package com.tsystems.converters;

import com.tsystems.dto.ProductOrder;
import com.tsystems.entity.ProductOrderEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductOrderConverter extends AbstractConverter<ProductOrderEntity, ProductOrder> {

    private final OrderConverter orderConverter;
    private final ProductConverter productConverter;

    @Autowired
    public ProductOrderConverter(OrderConverter orderConverter, ProductConverter productConverter) {
        this.orderConverter = orderConverter;
        this.productConverter = productConverter;
    }

    @Override
    public ProductOrder convertEntity(ProductOrderEntity entity) {
        ProductOrder productOrder = new ProductOrder();

        productOrder.setId(entity.getId());
        productOrder.setOrder(orderConverter.fromEntity(entity.getOrder()));
        productOrder.setProduct(productConverter.fromEntity(entity.getProduct()));
        productOrder.setQuantity(entity.getQuantity());

        return productOrder;
    }

    @Override
    public ProductOrderEntity convertDto(ProductOrder dto) {
        ProductOrderEntity entity = new ProductOrderEntity();

        entity.setId(dto.getId());
        entity.setOrder(orderConverter.fromDto(dto.getOrder()));
        entity.setProduct(productConverter.fromDto(dto.getProduct()));
        entity.setQuantity(dto.getQuantity());

        return entity;
    }
}
