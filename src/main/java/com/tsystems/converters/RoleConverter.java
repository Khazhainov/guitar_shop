package com.tsystems.converters;

import com.tsystems.dto.Role;
import com.tsystems.entity.RoleEntity;
import org.springframework.stereotype.Component;

@Component
public class RoleConverter extends AbstractConverter<RoleEntity, Role> {

    @Override
    public Role convertEntity(RoleEntity entity) {
        Role role = new Role();

        role.setId(entity.getId());
        role.setName(entity.getName());

        return role;
    }

    @Override
    public RoleEntity convertDto(Role dto) {
        RoleEntity entity = new RoleEntity();

        entity.setId(dto.getId());
        entity.setName(dto.getName());

        return entity;
    }

}
