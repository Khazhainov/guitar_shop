package com.tsystems.converters;

import com.tsystems.dto.User;
import com.tsystems.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserConverter extends AbstractConverter<UserEntity, User> {

    private final RoleConverter roleConverter;

    @Autowired
    public UserConverter(RoleConverter roleConverter) {
        this.roleConverter = roleConverter;
    }

    @Override
    public User convertEntity(UserEntity entity) {
        User user = new User();

        user.setId(entity.getId());
        user.setName(entity.getName());
        user.setLastName(entity.getLastName());
        user.setBirthday(entity.getBirthday());
        user.setEmail(entity.getEmail());
        user.setPassword(entity.getPassword());
        user.setRole(roleConverter.fromEntity(entity.getRole()));

        return user;
    }

    @Override
    public UserEntity convertDto(User dto) {
        UserEntity entity = new UserEntity();

        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setLastName(dto.getLastName());
        entity.setBirthday(dto.getBirthday());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setRole(roleConverter.fromDto(dto.getRole()));

        return entity;
    }
}
