package com.tsystems.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Repository
public interface AbstractDao<T extends Serializable> {

    @Transactional
    T update(T entity);

    @Transactional
    void create(T entity);

    @Transactional(readOnly = true)
    T findById(int id);

    @Transactional
    void deleteById(int id);

}
