package com.tsystems.dao;

import com.tsystems.dto.Address;
import com.tsystems.dto.User;
import com.tsystems.entity.AddressEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Repository
public interface AddressDao extends AbstractDao<AddressEntity> {

    @Transactional(readOnly = true)
    Collection<AddressEntity> findUserAddresses(User user);

}
