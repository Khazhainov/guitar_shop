package com.tsystems.dao;

import com.tsystems.entity.CategoryEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CategoryDao extends AbstractDao<CategoryEntity> {

    @Transactional(readOnly = true)
    List<CategoryEntity> findByCategoryName(String categoryName);

    @Transactional(readOnly = true)
    List<CategoryEntity> findAll();

}
