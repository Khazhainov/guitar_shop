package com.tsystems.dao;

import com.tsystems.entity.OrderEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface OrderDao extends AbstractDao<OrderEntity> {

    @Transactional(readOnly = true)
    List<OrderEntity> findAll();

    @Transactional(readOnly = true)
    List<OrderEntity> findOrdersPerPeriod(Date periodStart, Date periodEnd);

}
