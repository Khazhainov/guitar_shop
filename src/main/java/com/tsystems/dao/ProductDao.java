package com.tsystems.dao;

import com.tsystems.dto.TopProducts;
import com.tsystems.entity.ProductEntity;
import com.tsystems.enums.Brand;
import com.tsystems.enums.Color;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface ProductDao extends AbstractDao<ProductEntity> {

    @Transactional(readOnly = true)
    List<ProductEntity> findByBrand(Brand brand);

    @Transactional(readOnly = true)
    List<ProductEntity> findByColor(Color color);

    @Transactional(readOnly = true)
    List<ProductEntity> findByName(String productName);

    @Transactional(readOnly = true)
    List<ProductEntity> findAll();

    @Transactional(readOnly = true)
    Collection<TopProducts> getTopTenProducts();

}
