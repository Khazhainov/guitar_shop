package com.tsystems.dao;

import com.tsystems.dto.Role;
import com.tsystems.entity.RoleEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RoleDao extends AbstractDao<RoleEntity> {

    @Transactional(readOnly = true)
    List findByRoleName(String roleName);

    @Transactional(readOnly = true)
    List<RoleEntity> findAll();

}
