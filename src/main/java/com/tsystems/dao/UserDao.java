package com.tsystems.dao;

import com.tsystems.dto.TopUsers;
import com.tsystems.entity.UserEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Repository
public interface UserDao extends AbstractDao<UserEntity> {

    @Transactional(readOnly = true)
    UserEntity findByEmail(String email);

    @Transactional(readOnly = true)
    Collection<TopUsers> getTopTenClients();

    @Transactional(readOnly = true)
    List<UserEntity> findAll();

}
