package com.tsystems.dao.impl;

import com.tsystems.dao.AbstractDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

@Repository
public abstract class AbstractDaoImpl<T extends Serializable> implements AbstractDao<T> {

    //private final Logger log = LoggerFactory.getLogger(AbstractDaoImpl.class);

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public T update(T entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void create(T entity) { entityManager.persist(entity); }

    @Override
    public T findById(int id) {
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public void deleteById(int id) {
        T entity = findById(id);
        entityManager.remove(entity);
    }

    protected abstract Class<T> getEntityClass();

}
