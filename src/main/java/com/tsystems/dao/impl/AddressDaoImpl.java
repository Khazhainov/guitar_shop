package com.tsystems.dao.impl;

import com.tsystems.dao.AddressDao;
import com.tsystems.dto.Address;
import com.tsystems.dto.User;
import com.tsystems.entity.AddressEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Repository
public class AddressDaoImpl extends AbstractDaoImpl<AddressEntity> implements AddressDao {

    @Override
    protected Class<AddressEntity> getEntityClass() {
        return AddressEntity.class;
    }

    @Override
    public Collection<AddressEntity> findUserAddresses(User user) {
        return entityManager.createNativeQuery("SELECT * FROM address WHERE user_id = :userId", AddressEntity.class)
                .setParameter("userId", user.getId())
                .getResultList();
    }

}
