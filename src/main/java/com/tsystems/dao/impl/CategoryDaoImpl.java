package com.tsystems.dao.impl;

import com.tsystems.dao.CategoryDao;
import com.tsystems.entity.CategoryEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryDaoImpl extends AbstractDaoImpl<CategoryEntity> implements CategoryDao {

    @Override
    public List<CategoryEntity> findByCategoryName(String categoryName) {
        String sql = "SELECT c.name category, pd.brand, p.name model, pd.color FROM g_shop.category c " +
                "JOIN g_shop.product p ON c.id = p.category_id " +
                "JOIN g_shop.product_details pd ON p.id = pd.id WHERE c.name = :name";
        return entityManager.createNativeQuery(sql, getEntityClass())
                .setParameter("name", categoryName)
                .getResultList();
    }

    @Override
    public List<CategoryEntity> findAll() {
        String sql = "SELECT * FROM g_shop.category";
        return entityManager.createNativeQuery(sql, getEntityClass()).getResultList();
    }

    @Override
    protected Class<CategoryEntity> getEntityClass() {
        return CategoryEntity.class;
    }

}
