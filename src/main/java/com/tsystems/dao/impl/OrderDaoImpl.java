package com.tsystems.dao.impl;

import com.tsystems.dao.OrderDao;
import com.tsystems.entity.OrderEntity;
//import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class OrderDaoImpl extends AbstractDaoImpl<OrderEntity> implements OrderDao {

    //private final static Logger log = Logger.getLogger(OrderDaoImpl.class);

    @Override
    public List<OrderEntity> findAll() {
        //log.debug("Find all users.");
        String sql = "SELECT * FROM g_shop.order /*o JOIN g_shop.product_order po ON o.id = po.order_id*/";
        return entityManager.createNativeQuery(sql, getEntityClass()).getResultList();
    }

    @Override
    public List<OrderEntity> findOrdersPerPeriod(Date periodStart, Date periodEnd) {
        //log.debug("Get orders per period" + periodStart + "-" + periodEnd + ".");
        String sql = "SELECT * FROM g_shop.order o WHERE o.order_date >= :start AND o.order_date <= :end";
        return entityManager.createNativeQuery(sql, getEntityClass())
                .setParameter("start", periodStart)
                .setParameter("end", periodEnd)
                .getResultList();
    }

    @Override
    protected Class<OrderEntity> getEntityClass() {
        return OrderEntity.class;
    }
}
