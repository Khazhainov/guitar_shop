package com.tsystems.dao.impl;

import com.tsystems.dao.ProductDao;
import com.tsystems.dto.TopProducts;
import com.tsystems.entity.ProductEntity;
import com.tsystems.enums.Brand;
import com.tsystems.enums.Color;
//import org.apache.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ProductDaoImpl extends AbstractDaoImpl<ProductEntity> implements ProductDao {

    private final static Logger log = LoggerFactory.getLogger(ProductDaoImpl.class);

    @Override
    public List findByBrand(Brand brand) {
        //log.debug("Find products in the DB by brand.");
        String sql = "SELECT brand, name, color FROM g_shop.product_details pd " +
                "JOIN g_shop.product p USING (id) WHERE pd.brand = :brand";
        return entityManager.createNativeQuery(sql, getEntityClass())
                .setParameter("brand", brand)
                .getResultList();
    }

    @Override
    public List findByColor(Color color) {
        //log.debug("Find products in the DB by color.");
        String sql = "SELECT brand, name, color FROM g_shop.product_details pd " +
                "JOIN g_shop.product p USING (id) WHERE pd.color = :color";
        return entityManager.createNativeQuery(sql, getEntityClass())
                .setParameter("color", color)
                .getResultList();
    }

    @Override
    public List findByName(String productName) {
        //log.debug("Find products in the DB by name.");
        String sql = "SELECT brand, name, color FROM g_shop.product_details pd " +
                "JOIN g_shop.product p USING (id) WHERE p.name = :name";
        return entityManager.createNativeQuery(sql, getEntityClass())
                .setParameter("name", productName)
                .getResultList();
    }

    @Override
    public List<ProductEntity> findAll() {
        String sql = "SELECT * FROM g_shop.product p JOIN g_shop.product_details pd ON p.id = pd.id " +
                "JOIN g_shop.category c ON p.category_id = c.id";
        return entityManager.createNativeQuery(sql, getEntityClass()).getResultList();
    }

    @Override
    public Collection<TopProducts> getTopTenProducts() {

        String sql = "SELECT g_shop.product_order.product_id, SUM(g_shop.product_order.quantity) 'total quantity' " +
                "FROM g_shop.product_order GROUP BY product_id " +
                "ORDER BY SUM(g_shop.product_order.quantity) DESC LIMIT 10";

        List<Object[]> resultList = entityManager.createNativeQuery(sql).getResultList();
        /*log.error("result list: " + resultList.getClass())
        resultList.stream().forEach(obj -> Arrays.stream(obj).forEach(o -> log.error(o.getClass().getName())));
        */
        return resultList.stream()
                .map(data -> new TopProducts((Integer)data[0], ((BigDecimal)data[1]).intValue()))
                .collect(Collectors.toList());
    }

    @Override
    protected Class<ProductEntity> getEntityClass() {
        return ProductEntity.class;
    }
}
