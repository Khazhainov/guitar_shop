package com.tsystems.dao.impl;

import com.tsystems.dao.RoleDao;
import com.tsystems.entity.RoleEntity;
//import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleDaoImpl extends AbstractDaoImpl<RoleEntity> implements RoleDao {

    //private final static Logger log = Logger.getLogger(RoleDaoImpl.class);

    @Override
    protected Class<RoleEntity> getEntityClass() {
        return RoleEntity.class;
    }

    @Override
    public List findByRoleName(String roleName) {
        //log.debug("Find role by name.");
        String sql = "SELECT * FROM g_shop.role WHERE name = :name";
        return entityManager.createNativeQuery(sql, getEntityClass())
                .setParameter("name", roleName)
                .getResultList();
    }

    @Override
    public List<RoleEntity> findAll() {
        String sql = "SELECT * FROM g_shop.role";
        return entityManager.createNativeQuery(sql, getEntityClass()).getResultList();
    }

}
