package com.tsystems.dao.impl;

import com.tsystems.dao.UserDao;
import com.tsystems.dto.TopUsers;
import com.tsystems.entity.UserEntity;
//import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<UserEntity> implements UserDao {

    //private final static Logger log = Logger.getLogger(UserDaoImpl.class);

    @Override
    protected Class<UserEntity> getEntityClass() {
        return UserEntity.class;
    }

    @Override
    public UserEntity findByEmail(String email) {
        //log.debug("Find User by e-mail.");
        String sql = "SELECT * FROM g_shop.user WHERE email = :email";
        return (UserEntity) entityManager.createNativeQuery(sql,getEntityClass())
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public Collection<TopUsers> getTopTenClients() {
        String sql = "SELECT g_shop.order.user_id, SUM(g_shop.order.total_cost) FROM g_shop.order " +
                "GROUP BY user_id ORDER BY SUM(g_shop.order.total_cost) DESC LIMIT 10";
        List<Object[]> resultList = entityManager.createNativeQuery(sql).getResultList();

        return resultList.stream()
                .map(data -> new TopUsers(((Integer)data[0]), ((Double)data[1])))
                .collect(Collectors.toList());
    }

    @Override
    public List<UserEntity> findAll() {
        String sql = "SELECT * FROM g_shop.user";
        return entityManager.createNativeQuery(sql, getEntityClass()).getResultList();
    }

}
