package com.tsystems.dto;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class Address {

    private int id;
    @NotBlank
    private String country;
    @NotBlank
    private String city;
    @Min(0)
    private int postIndex;
    @NotBlank
    private String street;
    @NotBlank
    private String house;
    @Min(0)
    private int apartment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPostIndex() {
        return postIndex;
    }

    public void setPostIndex(int postIndex) {
        this.postIndex = postIndex;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public int getApartment() {
        return apartment;
    }

    public void setApartment(int apartment) {
        this.apartment = apartment;
    }

}
