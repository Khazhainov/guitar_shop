package com.tsystems.dto;

import java.util.Collection;

public class Cart {

    private Collection<ProductWithCount> products;
    private double totalPrice;

    public Cart(Collection<ProductWithCount> products, double totalPrice) {
        this.products = products;
        this.totalPrice = totalPrice;
    }

    public Collection<ProductWithCount> getProducts() {
        return products;
    }

    public double getTotalPrice() {
        return totalPrice;
    }
}
