package com.tsystems.dto;

public class CartProduct {

    private int productId;
    private int count;

    public int getProductId() {
        return productId;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "CartProduct{" +
                "productId=" + productId +
                ", count=" + count +
                '}';
    }
}
