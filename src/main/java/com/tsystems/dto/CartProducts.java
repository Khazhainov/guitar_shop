package com.tsystems.dto;

import java.util.Collection;

public class CartProducts {

    private Collection<CartProduct> products;

    public Collection<CartProduct> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "CartProducts{" +
                "products=" + products +
                '}';
    }
}
