package com.tsystems.dto;

import java.util.Collection;

public class FilterData {

    private Collection<String> brands;
    private Collection<String> models;
    private Collection<String> colors;
    private Collection<String> categories;
    private PriceInterval price;

    public FilterData() {}

    public Collection<String> getBrands() {
        return brands;
    }

    public Collection<String> getModels() {
        return models;
    }

    public Collection<String> getColors() {
        return colors;
    }

    public Collection<String> getCategories() {
        return categories;
    }

    public PriceInterval getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "FilterData{" +
                "brands=" + brands +
                ", models=" + models +
                ", colors=" + colors +
                ", categories=" + categories +
                ", price=" + price +
                '}';
    }
}
