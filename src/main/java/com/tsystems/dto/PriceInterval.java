package com.tsystems.dto;

public class PriceInterval {

    private double from;
    private double to;

    public double getFrom() {
        return from;
    }

    public double getTo() {
        return to;
    }

    @Override
    public String toString() {
        return "PriceInterval{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }

}
