package com.tsystems.dto;

public class ProductWithCount {

    private Product product;
    private int count;

    public ProductWithCount(Product product, int count) {
        this.product = product;
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public int getCount() {
        return count;
    }
}
