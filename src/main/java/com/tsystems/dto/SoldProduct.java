package com.tsystems.dto;

public class SoldProduct {

    private Product product;
    private int total;

    public SoldProduct(Product product, int total) {
        this.product = product;
        this.total = total;
    }

    public Product getProduct() {
        return product;
    }

    public int getTotal() {
        return total;
    }
}
