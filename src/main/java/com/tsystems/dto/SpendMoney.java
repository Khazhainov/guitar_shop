package com.tsystems.dto;

public class SpendMoney {

    private User user;
    private double total;

    public SpendMoney(User user, double total) {
        this.user = user;
        this.total = total;
    }

    public User getUser() {
        return user;
    }

    public double getTotal() {
        return total;
    }
}
