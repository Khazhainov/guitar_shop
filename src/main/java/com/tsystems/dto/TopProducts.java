package com.tsystems.dto;

public class TopProducts {

    private int productId;
    private int totalSold;

    public TopProducts(int productId, int totalSold) {
        this.productId = productId;
        this.totalSold = totalSold;
    }

    public int getProductId() {
        return productId;
    }

    public int getTotalSold() {
        return totalSold;
    }
}
