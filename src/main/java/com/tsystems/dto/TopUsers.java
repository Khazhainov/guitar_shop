package com.tsystems.dto;

public class TopUsers {

    private int userId;
    private double totalSpend;

    public TopUsers(int userId, double totalSpend) {
        this.userId = userId;
        this.totalSpend = totalSpend;
    }

    public int getUserId() {
        return userId;
    }

    public double getTotalSpend() {
        return totalSpend;
    }
}
