package com.tsystems.entity;

import com.tsystems.enums.DeliveryMethod;
import com.tsystems.enums.OrderStatus;
import com.tsystems.enums.PaymentMethod;
import com.tsystems.enums.PaymentStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "order", schema = "g_shop")
public class OrderEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "payment_method", columnDefinition = "ENUM ('CASH', 'CREDIT_CARD')", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;

    @Column(name = "delivery_method", columnDefinition = "ENUM ('DHL', 'PICKUP')", nullable = false)
    @Enumerated(EnumType.STRING)
    private DeliveryMethod deliveryMethod;

    @Column(name = "payment_status", columnDefinition = "ENUM ('AWAITING_PAYMENT', 'PAID')", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;

    @Column(name = "order_status", columnDefinition = "ENUM ('AWAITING_PAYMENT', 'WAITING_FOR_SHIPMENT', 'SHIPPED', 'DELIVERED')", nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Temporal(TemporalType.DATE)
    @Column(name = "order_date", nullable = false)
    private Date orderDate;

    @Column(name = "total_cost", nullable = false)
    private double totalCost;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Collection<ProductOrderEntity> items;

    //@OneToOne(cascade = CascadeType.ALL)
    @ManyToOne
    //@PrimaryKeyJoinColumn
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private AddressEntity address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public DeliveryMethod getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEntity that = (OrderEntity) o;
        return id == that.id &&
                Double.compare(that.totalCost, totalCost) == 0 &&
                paymentMethod == that.paymentMethod &&
                deliveryMethod == that.deliveryMethod &&
                paymentStatus == that.paymentStatus &&
                orderStatus == that.orderStatus &&
                Objects.equals(orderDate, that.orderDate) &&
                Objects.equals(items, that.items) &&
                Objects.equals(user, that.user) &&
                Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, paymentMethod, deliveryMethod, paymentStatus, orderStatus, orderDate, totalCost, items, user, address);
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "id=" + id +
                ", paymentMethod=" + paymentMethod +
                ", deliveryMethod=" + deliveryMethod +
                ", paymentStatus=" + paymentStatus +
                ", orderStatus=" + orderStatus +
                ", orderDate=" + orderDate +
                ", totalCost=" + totalCost +
                ", items=" + items +
                ", user=" + user +
                ", address=" + address +
                '}';
    }
}
