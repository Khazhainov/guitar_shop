package com.tsystems.entity;

import com.tsystems.enums.Brand;
import com.tsystems.enums.Color;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product_details", schema = "g_shop")
public class ProductDetailEntity implements Serializable {

    @Id
    private int id;

    @Column(name = "brand", columnDefinition = "ENUM ('GIBSON', 'FENDER', 'GRECO')", nullable = false)
    @Enumerated(EnumType.STRING)
    private Brand brand;

    @Column(name = "color", columnDefinition = "ENUM ('BLACK', 'RED', 'SUNBURST')", nullable = false)
    @Enumerated(EnumType.STRING)
    private Color color;

    @Column(name = "weight", nullable = false)
    private double weight;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "ProductDetailEntity{" +
                "id=" + id +
                ", brand=" + brand +
                ", color=" + color +
                ", weight=" + weight +
                '}';
    }
}
