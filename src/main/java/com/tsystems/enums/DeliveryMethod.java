package com.tsystems.enums;

public enum DeliveryMethod {
    DHL,
    PICKUP
}
