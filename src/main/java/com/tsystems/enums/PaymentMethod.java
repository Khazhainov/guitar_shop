package com.tsystems.enums;

public enum PaymentMethod {
    CASH,
    CREDIT_CARD
}
