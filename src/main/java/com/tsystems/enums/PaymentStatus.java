package com.tsystems.enums;

public enum PaymentStatus {
    AWAITING_PAYMENT,
    PAID,
}
