package com.tsystems.exception;

public class EmptyStringException extends Exception {

    public EmptyStringException(String message) {
        super(message);
    }
}
