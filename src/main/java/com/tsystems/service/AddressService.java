package com.tsystems.service;

import com.tsystems.dto.Address;
import com.tsystems.dto.User;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;

@Service
public interface AddressService {

    Address createAddress(Address address);

    Address updateAddress(Address address);

    Collection<Address> findUserAddresses(User user);

    Address findById(Integer id);

    void delete(Integer id);

}
