package com.tsystems.service;

import com.tsystems.dto.User;

public interface AuthenticationService {

    User getAuthenticatedUser();

    boolean isAuthenticated();

}
