package com.tsystems.service;

import com.tsystems.dto.*;

import java.util.Collection;

public interface CartService {

    Cart getProductsFromCart(CartProducts cartProducts);

}
