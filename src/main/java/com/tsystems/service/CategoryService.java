package com.tsystems.service;

import com.tsystems.dto.Category;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface CategoryService {

    Category createCategory(Category category);

    Collection<Category> findAll();

    Category findById(Integer id);

    void deleteCategory(Integer id);

}
