package com.tsystems.service;

import com.tsystems.dto.Order;
import com.tsystems.enums.OrderStatus;
import com.tsystems.enums.PaymentStatus;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface OrderService {

    Order findOrderById(Integer id);

    List<Order> findAll();

    List<Order> findOrdersPerPeriod(Date periodStart, Date periodEnd);

    Order createNewOrder(Order order);

    Order changeOrderStatus(Integer id, OrderStatus orderStatus, PaymentStatus paymentStatus);

}
