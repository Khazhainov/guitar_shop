package com.tsystems.service;

import com.tsystems.dto.FilterData;
import com.tsystems.dto.Product;

import java.util.Collection;

public interface ProductFilterService {

    Collection<Product> filter(FilterData filterData);

}
