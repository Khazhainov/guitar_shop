package com.tsystems.service;

import com.tsystems.dto.Product;
import com.tsystems.dto.SoldProduct;
import com.tsystems.entity.ProductEntity;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public interface ProductService {

    Product createProduct(Product product);

    Product findById(Integer id);

    void deleteById(Integer id);

    List<Product> findAll();

    Collection<SoldProduct> getTopTenProducts();

}
