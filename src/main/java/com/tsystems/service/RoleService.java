package com.tsystems.service;

import com.tsystems.dto.Role;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface RoleService {

    Role createRole(Role role);

    Role findByRoleName(String role);

    Collection<Role> findAll();

    Role findById(Integer id);

}
