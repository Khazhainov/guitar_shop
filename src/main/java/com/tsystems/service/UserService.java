package com.tsystems.service;

import com.tsystems.dto.SpendMoney;
import com.tsystems.dto.User;
import com.tsystems.entity.UserEntity;
import com.tsystems.exception.EmptyStringException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public interface UserService {

    User findByEmail(String email);

    User findById(Integer id);

    User createUser(User user);

    User updateUser(User user);

    Collection<User> findAll();

    void changePassword(User user, String password) throws EmptyStringException;

    Collection<SpendMoney> getTopTenClients();
}
