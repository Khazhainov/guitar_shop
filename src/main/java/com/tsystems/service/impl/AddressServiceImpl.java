package com.tsystems.service.impl;

import com.tsystems.converters.AddressConverter;
import com.tsystems.dao.AddressDao;
import com.tsystems.dao.UserDao;
import com.tsystems.dto.Address;
import com.tsystems.dto.User;
import com.tsystems.entity.AddressEntity;
import com.tsystems.service.AddressService;
import com.tsystems.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Service
public class AddressServiceImpl implements AddressService {

    private final UserDao userDao;
    private final AddressDao addressDao;

    private final AddressConverter addressConverter;
    private final AuthenticationService authService;

    @Autowired
    public AddressServiceImpl(UserDao userDao,
                              AddressDao addressDao,
                              AddressConverter addressConverter,
                              AuthenticationService authService) {
        this.userDao = userDao;
        this.addressDao = addressDao;
        this.addressConverter = addressConverter;
        this.authService = authService;
    }

    @Override
    public Address createAddress(Address address) {
        return save(address);
    }

    @Override
    public Address updateAddress(Address address) {
        return save(address);
    }

    @Override
    public Collection<Address> findUserAddresses(User user) {
        if (user == null) {
            return Collections.emptyList();
        }
        return addressDao.findUserAddresses(user).stream()
                .map(addressConverter::convertEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Address findById(Integer id) {
        if (id == null) {
            return null;
        }

        AddressEntity address = addressDao.findById(id);
        return addressConverter.fromEntity(address);
    }

    @Override
    public void delete(Integer id) {
        if (id != null) {
            addressDao.deleteById(id);
        }
    }

    private Address save(Address address) {
        User authenticatedUser = authService.getAuthenticatedUser();

        AddressEntity entity = addressConverter.fromDto(address);
        entity.setUser(userDao.findById(authenticatedUser.getId()));

        AddressEntity createdEntity = addressDao.update(entity);
        return addressConverter.fromEntity(createdEntity);
    }

}
