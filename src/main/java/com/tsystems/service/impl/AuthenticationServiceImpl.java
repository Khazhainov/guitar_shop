package com.tsystems.service.impl;

import com.tsystems.dto.User;
import com.tsystems.service.AuthenticationService;
import com.tsystems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserService userService;

    @Autowired
    public AuthenticationServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User getAuthenticatedUser() {
        if (isAuthenticated()) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            org.springframework.security.core.userdetails.User principal =
                    (org.springframework.security.core.userdetails.User) auth.getPrincipal();
            return userService.findByEmail(principal.getUsername());
        }
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return securityContext.getAuthentication() != null &&
                securityContext.getAuthentication().isAuthenticated() &&
                !(securityContext.getAuthentication() instanceof AnonymousAuthenticationToken);
    }

}
