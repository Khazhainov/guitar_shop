package com.tsystems.service.impl;

import com.tsystems.dto.*;
import com.tsystems.service.CartService;
import com.tsystems.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    private final ProductService productService;

    @Autowired
    public CartServiceImpl(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Cart getProductsFromCart(CartProducts cartProducts) {
        List<ProductWithCount> products = cartProducts.getProducts().stream()
                .map(cartProduct -> {
                    Product product = productService.findById(cartProduct.getProductId());
                    return new ProductWithCount(product, cartProduct.getCount());
                })
                .collect(Collectors.toList());

        double totalPrice = products.stream()
                .mapToDouble(product -> product.getProduct().getPrice() * product.getCount())
                .sum();

        return new Cart(products, totalPrice);
    }

}
