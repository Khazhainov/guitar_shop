package com.tsystems.service.impl;

import com.tsystems.converters.CategoryConverter;
import com.tsystems.dao.CategoryDao;
import com.tsystems.dto.Category;
import com.tsystems.entity.CategoryEntity;
import com.tsystems.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDao categoryDao;
    private final CategoryConverter categoryConverter;

    @Autowired
    public CategoryServiceImpl(CategoryDao categoryDao, CategoryConverter categoryConverter) {
        this.categoryDao = categoryDao;
        this.categoryConverter = categoryConverter;
    }

    @Override
    public Category createCategory(Category category) {
        CategoryEntity createdEntity = categoryDao.update(categoryConverter.convertDto(category));
        return categoryConverter.convertEntity(createdEntity);
    }

    @Override
    public Collection<Category> findAll() {
        return categoryDao.findAll().stream()
                .map(categoryConverter::convertEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Category findById(Integer id) {
        CategoryEntity entity = categoryDao.findById(id);
        return categoryConverter.convertEntity(entity);
    }

    @Override
    public void deleteCategory(Integer id) {
        categoryDao.deleteById(id);
    }
}
