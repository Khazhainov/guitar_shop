package com.tsystems.service.impl;

import com.tsystems.converters.OrderConverter;
import com.tsystems.dao.AddressDao;
import com.tsystems.dao.OrderDao;
import com.tsystems.dao.UserDao;
import com.tsystems.dto.Order;
import com.tsystems.entity.AddressEntity;
import com.tsystems.entity.OrderEntity;
import com.tsystems.entity.UserEntity;
import com.tsystems.enums.OrderStatus;
import com.tsystems.enums.PaymentStatus;
import com.tsystems.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderDao orderDao;
    private final UserDao userDao;
    private final AddressDao addressDao;

    private final OrderConverter orderConverter;

    @Autowired
    public OrderServiceImpl(OrderDao orderDao, UserDao userDao, AddressDao addressDao, OrderConverter orderConverter) {
        this.orderDao = orderDao;
        this.userDao = userDao;
        this.addressDao = addressDao;
        this.orderConverter = orderConverter;
    }

    @Override
    public Order findOrderById(Integer id) {
        OrderEntity orderEntity = orderDao.findById(id);
        return orderConverter.convertEntity(orderEntity);
    }

    @Override
    public List<Order> findAll() {
        return orderDao.findAll().stream()
                .map(orderConverter::convertEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> findOrdersPerPeriod(Date periodStart, Date periodEnd) {
        return orderDao.findOrdersPerPeriod(periodStart, periodEnd).stream()
                .map(orderConverter::convertEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Order createNewOrder(Order order) {
        UserEntity userEntity = userDao.findById(order.getUser().getId());
        AddressEntity addressEntity = addressDao.findById(order.getAddress().getId());

        OrderEntity entity = orderConverter.convertDto(order);
        entity.setUser(userEntity);
        entity.setAddress(addressEntity);

        OrderEntity createdEntity = orderDao.update(entity);
        return orderConverter.convertEntity(createdEntity);
    }

    @Override
    public Order changeOrderStatus(Integer id, OrderStatus orderStatus, PaymentStatus paymentStatus) {
        OrderEntity entity = orderDao.findById(id);
        entity.setOrderStatus(orderStatus);
        entity.setPaymentStatus(paymentStatus);

        OrderEntity createdEntity = orderDao.update(entity);
        return orderConverter.convertEntity(createdEntity);
    }

}
