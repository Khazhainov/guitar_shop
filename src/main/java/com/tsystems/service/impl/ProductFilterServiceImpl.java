package com.tsystems.service.impl;

import com.tsystems.dto.FilterData;
import com.tsystems.dto.Product;
import com.tsystems.service.ProductFilterService;
import com.tsystems.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class ProductFilterServiceImpl implements ProductFilterService {

    private final ProductService productService;

    @Autowired
    public ProductFilterServiceImpl(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Collection<Product> filter(FilterData filterData) {

        return productService.findAll()
                .stream()
                .filter(product -> {
                    if(filterData.getBrands().isEmpty()) return true;
                    else return filterData.getBrands().contains(product.getDetails().getBrand().name());
                })
                .filter(product -> {
                    if(filterData.getModels().isEmpty()) return true;
                    else return filterData.getModels().contains(product.getName());
                })
                .filter(product -> {
                    if(filterData.getColors().isEmpty()) return true;
                    else return filterData.getColors().contains(product.getDetails().getColor().name());
                })
                .filter(product -> {
                    if(filterData.getCategories().isEmpty()) return true;
                    else return filterData.getCategories().contains(product.getCategory().getName());
                })
                .filter(product -> {
                    if(filterData.getPrice().getFrom()==0.0) return true;
                    else return (product.getPrice() >= filterData.getPrice().getFrom());
                })
                .filter(product -> {
                    if(filterData.getPrice().getTo()==0.0) return true;
                    else return (product.getPrice() <= filterData.getPrice().getTo());
                })
                .collect(Collectors.toList());
    }
}
