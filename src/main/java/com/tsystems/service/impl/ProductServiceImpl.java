package com.tsystems.service.impl;

import com.tsystems.converters.ProductConverter;
import com.tsystems.converters.ProductDetailsConverter;
import com.tsystems.dao.CategoryDao;
import com.tsystems.dao.ProductDao;
import com.tsystems.dto.Product;
import com.tsystems.dto.SoldProduct;
import com.tsystems.dto.TopProducts;
import com.tsystems.entity.CategoryEntity;
import com.tsystems.entity.ProductDetailEntity;
import com.tsystems.entity.ProductEntity;
import com.tsystems.service.ProductService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    //private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductDao productDao;
    private final CategoryDao categoryDao;

    private final ProductConverter productConverter;
    private final ProductDetailsConverter detailsConverter;

    @Autowired
    public ProductServiceImpl(ProductDao productDao, ProductConverter productConverter, CategoryDao categoryDao, ProductDetailsConverter detailsConverter, ProductDetailsConverter detailsConverter1) {
        this.productDao = productDao;
        this.productConverter = productConverter;
        this.categoryDao = categoryDao;
        this.detailsConverter = detailsConverter1;
    }

    @Override
    public Product createProduct(Product product) {
        // TODO: validation (JSR-303)
        CategoryEntity categoryEntity = categoryDao.findById(product.getCategory().getId());

        ProductEntity entity = productConverter.fromDto(product);
        entity.setCategory(categoryEntity);

        productDao.create(entity);
        if (product.getDetails() != null) {
            ProductDetailEntity detailEntity = detailsConverter.fromDto(product.getDetails());
            detailEntity.setId(entity.getId());
            entity.setDetails(detailEntity);
        }
        return productConverter.fromEntity(entity);
    }

    @Override
    public Product findById(Integer id) {
        ProductEntity entity = productDao.findById(id);
        return productConverter.fromEntity(entity);
    }

    @Override
    public void deleteById(Integer id) {
        productDao.deleteById(id);
    }

    @Override
    public List<Product> findAll() {
        return productDao.findAll().stream()
                .map(productConverter::fromEntity)
//                .map(entity -> productConverter.fromEntity(entity))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<SoldProduct> getTopTenProducts() {
        return productDao.getTopTenProducts().stream()
                .map(top -> {
                    Product product = findById(top.getProductId());
                    return new SoldProduct(product, top.getTotalSold());
                }).collect(Collectors.toList());
    }

}
