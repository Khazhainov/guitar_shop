package com.tsystems.service.impl;

import com.tsystems.converters.RoleConverter;
import com.tsystems.dao.RoleDao;
import com.tsystems.dto.Role;
import com.tsystems.entity.RoleEntity;
import com.tsystems.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleDao roleDao;
    private final RoleConverter roleConverter;

    @Autowired
    public RoleServiceImpl(RoleDao roleDao, RoleConverter roleConverter) {
        this.roleDao = roleDao;
        this.roleConverter = roleConverter;
    }

    @Override
    public Role createRole(Role role) {
        RoleEntity entity = roleConverter.fromDto(role);

        RoleEntity createdRole = roleDao.update(entity);
        return roleConverter.fromEntity(createdRole);
    }

    @Override
    public Role findByRoleName(String role) {
        return null;
    }

    @Override
    public Collection<Role> findAll() {
        return roleDao.findAll()
                .stream()
                .map(roleConverter::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Role findById(Integer id) {
        RoleEntity entity = roleDao.findById(id);
        return roleConverter.fromEntity(entity);
    }
}
