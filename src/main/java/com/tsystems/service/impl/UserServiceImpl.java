package com.tsystems.service.impl;

import com.tsystems.converters.UserConverter;
import com.tsystems.dao.RoleDao;
import com.tsystems.dao.UserDao;
import com.tsystems.dto.SpendMoney;
import com.tsystems.dto.User;
import com.tsystems.entity.RoleEntity;
import com.tsystems.entity.UserEntity;
import com.tsystems.exception.EmptyStringException;
import com.tsystems.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final int USER_ROLE_ID = 2;

    private final UserDao userDao;
    private final UserConverter userConverter;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao,
                           UserConverter userConverter,
                           PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.userConverter = userConverter;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User findByEmail(String email) {
        return userConverter.fromEntity(userDao.findByEmail(email));
    }

    @Override
    public User findById(Integer id) {
        UserEntity entity = userDao.findById(id);
        return userConverter.fromEntity(entity);
    }

    @Override
    public User createUser(User user) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(USER_ROLE_ID);

        UserEntity entity = userConverter.fromDto(user);
        entity.setRole(roleEntity);
        entity.setPassword(passwordEncoder.encode(user.getPassword()));

        UserEntity createdEntity = userDao.update(entity);
        return userConverter.fromEntity(createdEntity);
    }

    @Override
    public User updateUser(User user) {
        UserEntity entity = userDao.findById(user.getId());

        entity.setName(user.getName());
        entity.setLastName(user.getLastName());
        entity.setPassword(user.getPassword());
        entity.setBirthday(user.getBirthday());
        entity.setEmail(user.getEmail());

        userDao.update(entity);

        return userConverter.convertEntity(entity);
    }

    @Override
    public Collection<User> findAll() {
        return userDao.findAll().stream()
                .map(userConverter::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public void changePassword(User user, String password) throws EmptyStringException {
        if (password == null || password.trim().isEmpty()) {
            throw new EmptyStringException("Password must be non null or empty");
        }

        user.setPassword(passwordEncoder.encode(password));

        UserEntity entity = userConverter.convertDto(user);
        userDao.update(entity);
    }

    @Override
    public Collection<SpendMoney> getTopTenClients() {
        return userDao.getTopTenClients().stream()
                .map(top -> {
                    User user = findById(top.getUserId());
                    return new SpendMoney(user, top.getTotalSpend());
                }).collect(Collectors.toList());
    }


}
