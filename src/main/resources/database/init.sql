DROP DATABASE IF EXISTS g_shop;
CREATE DATABASE IF NOT EXISTS g_shop;
USE g_shop;

CREATE TABLE IF NOT EXISTS role (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS category (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS product (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) UNIQUE NOT NULL,
  price DOUBLE NOT NULL,
  capacity DOUBLE NOT NULL,
  quantity INT NOT NULL,
  category_id INT NOT NULL,
  CONSTRAINT category_fk FOREIGN KEY (category_id) REFERENCES category(id)
);

CREATE TABLE IF NOT EXISTS product_details (
  id INT PRIMARY KEY,
  brand ENUM ('GIBSON', 'FENDER', 'GRECO') NOT NULL,
  color ENUM ('BLACK', 'RED', 'SUNBURST'),
  weight DOUBLE NOT NULL,
  CONSTRAINT product_fk FOREIGN KEY (id) REFERENCES product(id)
);

CREATE TABLE IF NOT EXISTS user (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(60) NOT NULL,
  last_name VARCHAR(60) NOT NULL,
  birthday DATE NOT NULL,
  email VARCHAR(100) UNIQUE NOT NULL,
  password VARCHAR(255) NOT NULL,
  role_id INT NOT NULL,
  CONSTRAINT role_id_fk FOREIGN KEY (role_id) REFERENCES role(id)
);

CREATE TABLE IF NOT EXISTS address (
  id INT PRIMARY KEY AUTO_INCREMENT,
  country VARCHAR(60) NOT NULL,
  city VARCHAR(100) NOT NULL,
  post_index INT NOT NULL,
  street VARCHAR(255) NOT NULL,
  house VARCHAR(100) NOT NULL,
  apartment INT NOT NULL,
  user_id INT NOT NULL,
  CONSTRAINT user_fk FOREIGN KEY (id) REFERENCES user(id)
);

CREATE TABLE IF NOT EXISTS `order` (
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_id INT NOT NULL,
  address_id INT NOT NULL,
  payment_method ENUM ('CASH', 'CREDIT CARD') NOT NULL,
  delivery_method ENUM ('DHL', 'PICKUP') NOT NULL,
  payment_status ENUM ('AWAITING PAYMENT', 'PAID') NOT NULL,
  order_status ENUM ('AWAITING PAYMENT', 'WAITING FOR SHIPMENT', 'SHIPPED', 'DELIVERED') NOT NULL,
  order_date DATE NOT NULL,
  total_cost DOUBLE NOT NULL,
  CONSTRAINT order_user_id_fk FOREIGN KEY (user_id) REFERENCES user(id),
  CONSTRAINT order_user_address_id_fk FOREIGN KEY (address_id) REFERENCES address(id)
);

CREATE TABLE IF NOT EXISTS product_order (
  id INT PRIMARY KEY AUTO_INCREMENT,
  order_id INT NOT NULL,
  product_id INT NOT NULL,
  quantity INT NOT NULL,
  CONSTRAINT order_product_order_id_fk FOREIGN KEY (order_id) REFERENCES `order`(id),
  CONSTRAINT order_product_product_id_fk FOREIGN KEY (product_id) REFERENCES product(id)
);
