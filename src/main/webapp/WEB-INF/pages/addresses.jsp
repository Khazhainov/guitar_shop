<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/centralized-forms.css"/>" rel="stylesheet">

        <title>Add address</title>
    </head>

    <body>

        <sec:authorize access="isAnonymous()">
            Anonymous users cannot access the information provided on this page. Please, log in first.
        </sec:authorize>

        <sec:authorize access="isAuthenticated()">

            <div align="center">
                <h1>Create new address</h1>

                <form:form action="/address" method="POST" cssClass="g-shop-form" modelAttribute="address">
                    <div class="form-group">
                        <div class="inline">
                            <label>Country</label>
                            <form:input type="text" class="form-control" name="country" path="country"/>
                        </div>
                        <form:errors path="country" cssClass="error" />
                    </div>

                    <div class="form-group">
                        <div class="inline">
                            <label>City</label>
                            <form:input type="text" class="form-control" name="city" path="city" />
                        </div>
                        <form:errors path="city" cssClass="error" />
                    </div>

                    <div class="form-group">
                        <div class="inline">
                            <label>Post index</label>
                            <form:input type="number" class="form-control" name="postIndex" path="postIndex" />
                        </div>
                        <form:errors path="postIndex" cssClass="error" />
                    </div>

                    <div class="form-group">
                        <div class="inline">
                            <label>Street</label>
                            <form:input type="text" class="form-control" name="street" path="street" />
                        </div>
                        <form:errors path="street" cssClass="error" />
                    </div>

                    <div class="form-group">
                        <div class="inline">
                            <label>House number</label>
                            <form:input type="text" class="form-control" name="house" path="house" />
                        </div>
                        <form:errors path="house" cssClass="error" />
                    </div>

                    <div class="form-group">
                        <div class="inline">
                            <label>Apartment</label>
                            <form:input type="number" class="form-control" name="apartment" path="apartment" />
                        </div>
                        <form:errors path="apartment" cssClass="error" />
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form:form>

            </div>
        </sec:authorize>

    </body>

</html>
