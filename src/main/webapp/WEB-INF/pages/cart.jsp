<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/cart.css"/>" rel="stylesheet">
        <script src="<c:url value="/resources/js/cart.js"/>" ></script>
        <script src="<c:url value="/resources/js/cart-storage.js"/>" ></script>

        <title>Cart</title>
    </head>

    <body>

    <h1 class="padding">Cart</h1>

    <a class="padding alert-link" href="/products">
        Products
    </a>

    <c:choose>

        <c:when test="${cart.products.size() > 0}">
            <div id="product-container">
                <div class="padding">
                    Total price: <span id="total-price">${cart.totalPrice}</span>
                </div>

                <div id="table-container">


                    <table class="table table-bordered" id="product-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Count</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="product" items="${cart.products}">
                            <tr product-id="${product.product.id}">
                                <td>${product.product.id}</td>
                                <td>${product.product.details.brand}</td>
                                <td>${product.product.name}</td>
                                <td>${product.count}</td>
                                <td>
                                    <button class="btn btn-primary remove-from-cart" product-id="${product.product.id}">Remove from cart</button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </c:when>

        <c:otherwise>
            <div class="message padding">
                You cart is empty. Please, select products first.
            </div>
        </c:otherwise>

    </c:choose>

    </body>
</html>
