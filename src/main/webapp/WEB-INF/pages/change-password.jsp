<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/centralized-forms.css"/>" rel="stylesheet">


        <title>Change password</title>
    </head>

    <body>

    <div align="center">
        <h1>User Login Form</h1>
        <form:form action="/change/password" method="post" class="g-shop-form">
            <div class="form-group inline">
                <label for="password">New password</label>
                <input id="password" name="password" type="password" class="form-control" />
            </div>

            <button class="btn btn-primary">Submit</button>
        </form:form>
    </div>

    </body>

</html>
