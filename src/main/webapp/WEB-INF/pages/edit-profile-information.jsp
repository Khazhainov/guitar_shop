<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/centralized-forms.css"/>" rel="stylesheet">


        <title>Edit information</title>
    </head>

    <body>

        <div align="center">
            <h2>Edit information</h2>
            <form:form action="/profile/edit" method="POST" cssClass="g-shop-form" modelAttribute="user">
                <form:input type="hidden" name="id" path="id" />
                <form:input type="hidden" name="email" path="email" />
                <form:input type="hidden" name="password" path="password" />
                <div class="form-group">
                    <div class="inline">
                        <label>First Name</label>
                        <form:input type="text" class="form-control" name="name" path="name"/>
                    </div>
                    <form:errors path="name" cssClass="error" />
                </div>
                <div class="form-group">
                    <div class="inline">
                        <label>Last Name</label>
                        <form:input type="text" class="form-control" name="lastName" path="lastName" />
                    </div>
                    <form:errors path="lastName" cssClass="error" />
                </div>
                <div class="form-group">
                    <div class="inline">
                        <label>Birthday</label>
                        <form:input type="date" class="form-control" name="birthday" path="birthday" />
                    </div>
                    <form:errors path="birthday" cssClass="error" />
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form:form>
        </div>

    </body>

</html>
