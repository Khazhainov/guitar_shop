<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/centralized-forms.css"/>" rel="stylesheet">

        <title>Login</title>
    </head>

    <body>
    <div align="center">
        <h1>User Login Form</h1>
        <form:form action="/login" method="post" class="g-shop-form">
            <div class="form-group inline">
                <label for="email">Email</label>
                <input name="username" id="email" type="text" class="form-control"/>
            </div>
            <div class="form-group inline">
                <label for="password">Password</label>
                <input id="password" name="password" type="password" class="form-control" />
            </div>

            <button class="btn btn-primary">Submit</button>
        </form:form>
    </div>
    </body>

</html>
