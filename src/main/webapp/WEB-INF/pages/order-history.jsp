<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/user-profile.css"/>" rel="stylesheet">

        <title>Order history</title>

    </head>

    <body>
        <h1 class="header-text">Order history</h1>

        <div id="user-actions">
            <div class="action">
                <a href="/profile" class="btn btn-primary">Return</a>
            </div>
        </div>

        <div id="table-container">
            <table class="table table-bordered" id="product-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>User email</th>
                    <th>Payment method</th>
                    <th>Delivery method</th>
                    <th>Payment status</th>
                    <th>Order status</th>
                    <th>Order date</th>
                    <th>Total cost</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="order" items="${orders}">
                    <tr product-id="${order.id}">
                        <td>${order.id}</td>
                        <td>${order.user.email}</td>
                        <td>${order.paymentMethod}</td>
                        <td>${order.deliveryMethod}</td>
                        <td>${order.paymentStatus}</td>
                        <td>${order.orderStatus}</td>
                        <td>${order.orderDate}</td>
                        <td>$ ${order.totalCost}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

        </div>

    </body>
</html>
