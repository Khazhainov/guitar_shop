<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/address.css"/>" rel="stylesheet">

        <title>Create order</title>
    </head>

    <body>

        <sec:authorize access="isAnonymous()">
            Anonymous users cannot access the information provided on this page. Please, log in first.
        </sec:authorize>

        <sec:authorize access="isAuthenticated()">


            <%--<div class="address-container">

                <c:forEach var="address" items="${addresses}">
                    <div class="address-info">
                        <div class="addr-el">${address.postIndex}</div>
                        <div class="addr-el">${address.country}</div>
                        <div class="addr-el">${address.city}</div>
                        <div class="addr-el">${address.street} st.</div>
                        <div class="addr-el">${address.house}</div>
                        <div class="addr-el">ap. ${address.apartment}</div>
                    </div>
                </c:forEach>

            </div>
--%>
        </sec:authorize>

    </body>

</html>
