<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>


        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/product.css"/>" rel="stylesheet">
        <script src="<c:url value="/resources/js/filters.js"/>" ></script>
        <script src="<c:url value="/resources/js/cart-storage.js"/>" ></script>

        <title>Products</title>

    </head>

    <body>
    <h1 class="header-text">Guitar Shop</h1>

    <div class="user-container">
        <div class="left-container">
            <sec:authorize access="isAnonymous()">
                <a href="/login" class="alert-link">Login</a>
                <a href="/registration" class="alert-link">Registration</a>
            </sec:authorize>
        </div>
        <div class="right-container">
            <a href="/cart" class="alert-link">
                Cart
            </a>
            <sec:authorize access="isAuthenticated()">
                <a href="/profile" class="alert-link">${user.name} ${user.lastName}</a>
            </sec:authorize>
        </div>
    </div>

    <div id="filter-container">

        <div class="header-line">
            <div class="filter">
                <select id="brand-filter" class="form-control" placeholder="Brand" multiple="multiple">
                    <c:forEach var="brand" items="${brands}">
                        <option value="${brand.name()}">${brand.name()}</option>
                    </c:forEach>
                </select>
                <select id="model-filter" class="from-control" placeholder="Model" multiple="multiple">
                    <c:forEach var="model" items="${models}">
                        <option value="${model.getName()}">${model.getName()}</option>
                    </c:forEach>
                </select>
                <select id="color-filter" class="from-control" placeholder="Color" multiple="multiple">
                    <c:forEach var="color" items="${colors}">
                        <option value="${color.name()}">${color.name()}</option>
                    </c:forEach>
                </select>
                <select id="category-filter" class="from-control" placeholder="Category" multiple="multiple">
                    <c:forEach var="category" items="${categories}">
                        <option value="${category.getName()}">${category.getName()}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="header-line">
            <input class="form-control m-width" id="from-price" placeholder="Price from"/>
            <input class="form-control m-width" id="to-price" placeholder="To price"/>
        </div>
        <div class="header-line errors" id="price-errors">

        </div>

        <div class="header-line">
            <button class="btn btn-primary" id="filter-submit">Filter</button>
            <button class="btn btn-primary" id="filter-reset">Reset filter</button>
        </div>

    </div>

    <div id="table-container">
        <table class="table table-bordered" id="product-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Brand</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="product" items="${products}">
                    <tr product-id="${product.id}">
                        <td>${product.id}</td>
                        <td>${product.details.brand}</td>
                        <td>${product.name}</td>
                        <td>${product.details.color}</td>
                        <td>$ ${product.price}</td>
                        <td>${product.quantity} EA</td>
                        <td>${product.category.name}</td>
                        <td>
                            <c:if test="${product.quantity > 0}">
                                <button type="submit" class="btn btn-primary add-to-cart" product-id="${product.id}">Add to cart</button>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </div>
    </body>

</html>