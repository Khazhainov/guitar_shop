<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/centralized-forms.css"/>" rel="stylesheet">

        <title>Registration</title>

    </head>

    <body>
        <div align="center">
            <h2>User Register Form</h2>
            <form:form action="/registration" method="POST" cssClass="g-shop-form" modelAttribute="user">
                <div class="form-group">
                    <div class="inline">
                        <label>First Name</label>
                        <form:input type="text" class="form-control" name="name" path="name"/>
                    </div>
                    <form:errors path="name" cssClass="error" />
                </div>
                <div class="form-group">
                    <div class="inline">
                        <label>Last Name</label>
                        <form:input type="text" class="form-control" name="lastName" path="lastName" />
                    </div>
                    <form:errors path="lastName" cssClass="error" />
                </div>
                <div class="form-group">
                    <div class="inline">
                        <label>Birthday</label>
                        <form:input type="date" class="form-control" name="birthday" path="birthday" />
                    </div>
                    <form:errors path="birthday" cssClass="error" />
                </div>
                <div class="form-group">
                    <div class="inline">
                        <label>Email address</label>
                        <form:input type="email" class="form-control" name="email" path="email" />
                    </div>
                    <form:errors path="email" cssClass="error" />
                </div>
                <div class="form-group">
                    <div class="inline">
                        <label>Password</label>
                        <form:input type="password" class="form-control" name="password" path="password" />
                    </div>
                    <form:errors path="password" cssClass="error" />
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form:form>
        </div>
    </body>
</html>

