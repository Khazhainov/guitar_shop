<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
    <head>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/user-profile.css"/>" rel="stylesheet">

        <title>Statistics</title>
    </head>
    <body>
        <sec:authorize access="isAnonymous()">
            Anonymous users cannot access the information provided on this page. Please, log in first.
        </sec:authorize>

        <sec:authorize access="isAuthenticated()">

            <div id="user-actions">
                <div class="action">
                    <a href="/profile" class="btn btn-primary">Return</a>
                </div>
            </div>

            <h1 class="header-text">Top Ten Products</h1>
            <div id="table-container">

                <table class="table table-bordered" id="top-products">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Color</th>
                            <th>Category</th>
                            <th>Total sold</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="product" items="${topProducts}">
                            <tr product-id="${product.product.id}">
                                <td>${product.product.id}</td>
                                <td>${product.product.details.brand}</td>
                                <td>${product.product.name}</td>
                                <td>${product.product.details.color}</td>
                                <td>${product.product.category.name}</td>
                                <td>${product.total}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <h1 class="header-text">Top Ten Clients</h1>
                <table class="table table-bordered" id="top-clients">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Last name</th>
                        <th>Birthday</th>
                        <th>Email</th>
                        <th>Total spend</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="client" items="${topClients}">
                        <tr product-id="${client.user.id}">
                            <td>${client.user.id}</td>
                            <td>${client.user.name}</td>
                            <td>${client.user.lastName}</td>
                            <td>${client.user.birthday}</td>
                            <td>${client.user.email}</td>
                            <td>$ ${client.total}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </sec:authorize>
    </body>
</html>
