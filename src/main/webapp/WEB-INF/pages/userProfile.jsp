<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/user-profile.css"/>" rel="stylesheet">

    <script src="<c:url value="/resources/js/address-manager.js"/>" ></script>

    <title>User profile</title>

    </head>

    <body>

    <sec:authorize access="isAnonymous()">
        Anonymous users cannot access the information provided on this page. Please, log in first.
    </sec:authorize>

    <sec:authorize access="isAuthenticated()">

        <div id="user-actions">

            <div class="action">
                <a href="/profile/edit" class="btn btn-primary">Edit information</a>
            </div>

            <div class="action">
                <a href="/change/password/" class="btn btn-primary">Change password</a>
            </div>

            <div class="action">
                <a href="/address" class="btn btn-primary">Register new address</a>
            </div>

            <div class="action">
                <a href="/products" class="btn btn-primary">Return to shopping</a>
            </div>

            <div class="action">
                <a href="/logout" class="btn btn-primary">Logout</a>
            </div>

        </div>

        <div id="user-info">

            <div class="info-block">
                <strong>Name:</strong> ${user.name}
            </div>

            <div class="info-block">
                <strong>Last name:</strong> ${user.lastName}
            </div>

            <div class="info-block">
                <strong>Email:</strong> ${user.email}
            </div>

            <div class="info-block">
                <strong>Birthday:</strong> ${user.birthday}
            </div>

            <div class="info-block">
                <strong>Role:</strong> ${user.role.name}
            </div>

            <c:if test="${addresses.size() > 0}">
                <div class="user-info address-container">

                    <h3>User addresses</h3>

                    <table class="table table-bordered" id="product-table">
                        <thead>
                        <tr>
                            <th>Country</th>
                            <th>City</th>
                            <th>Street</th>
                            <th>Post index</th>
                            <th>House</th>
                            <th>Apartment</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="address" items="${addresses}">
                            <tr>
                                <td>${address.country}</td>
                                <td>${address.city}</td>
                                <td>${address.street}</td>
                                <td>${address.postIndex}</td>
                                <td>${address.house}</td>
                                <td>${address.apartment}</td>
                                <td>
                                    <div class="button-container">
                                        <a href="/address/edit/${address.id}" class="btn btn-primary edit-address">Edit address</a>
                                        <button class="btn btn-primary remove-address" address-id="${address.id}">Remove address</button>
                                    </div>
                                </td>
                            </tr>

                        </c:forEach>
                        </tbody>
                    </table>

                </div>
            </c:if>

        </div>
    </sec:authorize>

   <%-- <h2>Address Register Form</h2>
    <form:form action="/profile" method="POST" cssClass="g-shop-form" modelAttribute="address">
        <div class="form-group">
            <div class="inline">
                <label>Country</label>
                <form:input type="text" class="form-control" name="country" path="country"/>
            </div>
            <form:errors path="country" cssClass="error" />
        </div>
        <div class="form-group">
            <div class="inline">
                <label>City</label>
                <form:input type="text" class="form-control" name="city" path="city" />
            </div>
            <form:errors path="city" cssClass="error" />
        </div>
        <div class="form-group">
            <div class="inline">
                <label>Post index</label>
                <form:input type="number" class="form-control" name="postIndex" path="postIndex" />
            </div>
            <form:errors path="postIndex" cssClass="error" />
        </div>
        <div class="form-group">
            <div class="inline">
                <label>Street</label>
                <form:input type="text" class="form-control" name="street" path="street" />
            </div>
            <form:errors path="street" cssClass="error" />
        </div>
        <div class="form-group">
            <div class="inline">
                <label>House number</label>
                <form:input type="text" class="form-control" name="house" path="house" />
            </div>
            <form:errors path="house" cssClass="error" />
        </div>
        <div class="form-group">
            <div class="inline">
                <label>Apartment</label>
                <form:input type="number" class="form-control" name="apartment" path="apartment" />
            </div>
            <form:errors path="apartment" cssClass="error" />
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form:form>--%>

    </body>
</html>
