$(document).ready(function() {

    $(document).on('click', 'button.remove-address', function() {
        const $btn = $(this);
        const addressId = $btn.attr('address-id');
        $.ajax({
            url: '/address/' + addressId,
            type: 'DELETE'
        }).done(function() {
            $btn.parent().parent().parent().remove();
            hideTableIsEmpty();
        });
    });

    function hideTableIsEmpty() {
        // noinspection JSJQueryEfficiency
        const rowCount = $('.address-container').find('tbody tr').length;
        if (rowCount === 0) {
            $('.address-container').addClass('hidden');
        }
    }

});