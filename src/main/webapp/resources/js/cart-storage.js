$(document).ready(function() {

    const current = $.cookie('cart');
    if (current === undefined) {
        $.cookie('cart', JSON.stringify({
            products: []
        }))
    }

    $(document).on('click', 'button.add-to-cart', function() {
        const $btn = $(this);
        const productId = $btn.attr('product-id');

        const cart = JSON.parse($.cookie('cart'));

        const product = cart.products.filter(function(el) {
            return el.productId === productId;
        });
        if (product.length === 0) {
            cart.products.push({
                productId: productId,
                count: 1
            });
        } else {
            product[0].count += 1;
        }

        $.cookie('cart', JSON.stringify(cart));

    });

});