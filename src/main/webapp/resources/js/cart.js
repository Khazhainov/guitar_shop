$(document).ready(function (){

    $(document).on('click', '.remove-from-cart', function() {
        const $btn = $(this);
        const productId = $btn.attr('product-id');

        const products = JSON.parse($.cookie('cart')).products.filter(function(el) {
            return el.productId !== productId;
        });

        const data = JSON.stringify({
            products: products
        });

        $.ajax({
            url: '/cart/recalculate',
            type: 'POST',
            contentType: 'application/json',
            data: data
        }).done(function(response) {
            $.cookie('cart', data);
            $btn.parent().parent().remove();
            $('#total-price').text(response.totalPrice);

            displayMessageIfTableIsEmpty();
        });

    });

    function displayMessageIfTableIsEmpty() {
        const rowCount = $('#product-table').find('tbody').find('tr').length;
        if (rowCount === 0) {
            $('#product-container').hide();
            $('body').append(
                $('<div>').addClass('message padding').text('You cart is empty. Please, select products first.')
            );
        }
    }

});