$(document).ready(function () {

    $("#brand-filter").select2({
        placeholder: 'Brand',
        width: '200px',
        multiple: true
    });

    $("#model-filter").select2({
        placeholder: 'Model',
        width: '200px',
        multiple: true
    });

    $("#color-filter").select2({
        placeholder: 'Color',
        width: '200px',
        multiple: true
    });

    $("#category-filter").select2({
        placeholder: 'Category',
        width: '200px',
        multiple: true
    });

    $("#filter-submit").on('click', function() {
        const filterData = collectData();

        const from = +filterData.price.from;
        const to = +filterData.price.to;
        if (isNaN(from) || isNaN(to) || from < 0 || to < 0 || !checkInterval(from, to)) {
            $('#price-errors').show().text('From and To prices must be positive numbers and "From" must be less than "To".');
            return;
        }

        $('#price-errors').hide();

        $.ajax({
            url: '/filter',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(filterData)
        }).done(function(products) {
            displayProducts(products);
        }).fail(function() {
            console.log('fail');
        });
    });

    $('#filter-reset').on('click', function() {
        clear('#brand-filter');
        clear('#model-filter');
        clear('#color-filter');
        clear('#category-filter');

        $.ajax({
            url: '/product',
            type: 'GET'
        }).done(function(products){
            displayProducts(products);
        });
    });

    function collectData() {
        return {
            brands: $('#brand-filter').select2('val'),
            models: $('#model-filter').select2('val'),
            colors: $('#color-filter').select2('val'),
            categories: $('#category-filter').select2('val'),
            price: {
                from: $('#from-price').val().replace(',', '.'),
                to: $('#to-price').val().replace(',', '.')
            }
        }
    }

    function displayProducts(products) {
        $('#product-table').find('tbody').find('tr').remove();

        products.forEach(function(it) {
            $('#product-table tbody').append(createRow(it));
        });

    }

    function createRow(product) {
        return $('<tr>').attr('product-id', product.id)
            .append($('<td>').text(product.id))
            .append($('<td>').text(product.details.brand))
            .append($('<td>').text(product.name))
            .append($('<td>').text(product.details.color))
            .append($('<td>').text(product.price))
            .append($('<td>').text(product.quantity))
            .append($('<td>').text(product.category.name))
            .append($('<td>')
                .append($('<button>').attr('product-id', product.id).addClass('btn btn-primary add-to-cart').text('Add to cart')))
    }

    function clear(selector) {
        $(selector).val(null).trigger('change');
    }

    function checkInterval(from, to) {
        if (from === to) {
            return true;
        }

        if (from > 0 && to === 0) {
            return true;
        }

        if (from > 0 && from < to) {
            return true;
        }

        return from === 0 && to > 0;
    }

    function clearInput(selector) {
        $(selector).val('');
    }

});